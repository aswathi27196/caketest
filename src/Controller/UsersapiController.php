<?php
//namespace App\Controller;

//use App\Controller\AppController;

  namespace App\Controller;
  use App\Controller\AppController;
  use Cake\ORM\TableRegistry;
  use Cake\Datasource\ConnectionManager;
  use Cake\Auth\DefaultPasswordHasher;
  use Cake\EmailConfig;

/**
 * AddToCart Controller
 *
 * @property \App\Model\Table\AddToCartTable $AddToCart
 *
 * @method \App\Model\Entity\AddToCart[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersapiController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function initialize(){
   		 parent::initialize();
       $this->loadComponent('RequestHandler');
       $this->loadModel('Users');
      //$this->loadHelper('EncodeHelper');
   	}

    
    //function to load register api
    //input:name,email,password 
    //output:insert the user record

    public function addapi(){ 
     
      if($_SERVER['REQUEST_METHOD']){
        
          $name = $this->request->data('name');
          $email = $this->request->data('email');
          $password = password_hash($this->request->data('password'), PASSWORD_DEFAULT);

          if(!empty($this->request->data('upload'))){
          
            $file = $this->request->data('upload'); //put the data into a var for easy use
            $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
            $arr_ext = array('jpg', 'jpeg', 'gif');
            //only process if the extension is valid
            if(in_array($ext, $arr_ext)){

              move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
              $img =  $this->request->data['uploadimage']= $file['name'];
            }
          }
          //using model
          $users = $this->Users->newEntity();
          $users->name = $name;
          $users->email = $email;
          $users->password = $password;
          $users->profileimage = $img;
        if($this->Users->save($users)){
           
          $result['status'] = true;
          $result['Message'] = "user added successfully";
        }
      }else{
        $result['status'] = false;
        $result['Message'] = "user added failed";
      }
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        echo json_encode($result);
    }


    //function to load update api
    //input:name,email
    //output:update the user record

    public function editapi($id){
        $exists = $this->Users->exists(['id' => $id]);
        
        if($exists){
          $user = $this->Users->get($id, [
              'contain' => []
          ]);
          //if($this->request->is(['patch', 'post', 'put'])){
          if($_SERVER['REQUEST_METHOD']){
          // print_r($this->request->getData());exit;
            

            if(!empty($this->request->data('upload'))){
          
              $file = $this->request->data('upload'); //put the data into a var for easy use
              $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
              $arr_ext = array('jpg', 'jpeg', 'gif');
              //only process if the extension is valid
              if(in_array($ext, $arr_ext)){

                move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
                $data['uploadimage'] =  $this->request->data['uploadimage']= $file['name'];
              }
            }
            //$user->profileimage = $img;
            $user = $this->Users->patchEntity($user, $this->request->getData(),$data);
            if($this->Users->save($user)) {
              $result['status'] = true;
              $result['Message'] = "user updated successfully";
            }else{
              $result['status'] = false;
              $result['Message'] = "user updated failed";
            }
          }else{
            $result['status'] = false;
            $result['Message'] = "data not found";
          }
        }else{
          $result['status'] = false;
          $result['Message'] = "record not found";
        }
        $this->set(compact('user'));
        $this->RequestHandler->respondAs('json');
        $this->response->getType('application/json');
        echo json_encode($result);
    }



    //function to load delete api
    //input:user id
    //output:delete the particular user record

    public function deleteapi($id){ 
     
      $exists = $this->Users->exists(['id' => $id]);
      if($exists){
        if($_SERVER['REQUEST_METHOD']){
          
          $users = $this->Users->get($id, [
                'contain' => []
            ]);
            //print_r($users);exit;
            $delete_user =  $this->Users->delete($users); 
            if($delete_user){
              $result['status'] = true;
              $result['Message'] = "user deleted successfully";
            
            }else{
              $result['status'] = false;
              $result['Message'] = "failed to delete the user";
            }
         }else{
            $result['status'] = false;
            $result['Message'] = "Invalid Request";
         }
       }else{
          $result['status'] = false;
          $result['Message'] = "record not found";
       }
      $this->RequestHandler->respondAs('json');
      $this->response->getType('application/json');
      echo json_encode($result);
    } 



    //function to load email api
    //input:from email,to email,subject
    //output:send email to the to email address
    
    public function emailsend(){

      //$App::uses('CakeEmail', 'Network/Email');
      $email = new EmailConfig('smtp');
      $email->from(array('sender@example.com' => 'test'));
      $email->to(array('aswathi.shenll@gmail.com' => 'aswathi'));
      $email->subject('test mail');
      $email->emailFormat('both'); 
      // both = html + text.

      //$email->template('template');
      if($email->send()) {
        $result['status'] = true;
        $result['Message'] = "email send successfully";
      } else {
          // echo $email->smtpError;
        $result['status'] = false;
        $result['Message'] = "smtpError";
      } 
      $this->RequestHandler->respondAs('json');
      $this->response->getType('application/json');
      echo json_encode($result);
    }


    public function login(){
     // print_r($_POST);exit;
      if($this->request->is('post')){
        $user = $this->Auth->identify();
       if($user) {
          $this->Auth->setUser($user);
          $session = $this->request->session();
          $session->write('users',$user);
          $result['status'] = true;
          $result['Message'] = "login successfully";
        }else{
          $result['status'] = false;
          $result['Message'] = "Invalid email or password";
        }
      }else{
        $result['status'] = false;
        $result['Message'] = "Post date empty";
      }
      $this->RequestHandler->respondAs('json');
      $this->response->getType('application/json');
      echo json_encode($result);
    }


    public function forgotpassword(){
       //print_r($_POST);
      if($this->request->is('post')) {
           $email = $this->request->data['email'];

          
           $user = $this->Users->find()->where(['email' => $email ])->first();

           if (!$user) {
               $this->Flash->error(__('No user with that email found.'));
               return $this->redirect(['controller' => 'Users','action' => 'forgotpassword']);

          }else{

               return $this->redirect(['controller' => 'Users','action' => 'changepassword']);
          }
      }
    }

    public function changepassword(){

      if($this->request->is('post')) {
        $id=  $this->Auth->user('id');
        $users = $this->Users->get($id, [
          'contain' => []
        ]);
        $data['id'] = $id;
       // print_r($data);exit;
        $data = $this->request->getData();
        //print_r($data);exit;
        if($data['password'] == $data['confpass']){
           $users = $this->Users->patchEntity($users, $data);
          if ($this->Users->save($users)) {
            $result['status'] = true;
            $result['Message'] = "password changed successfully";
          }else{
              $result['status'] = true;
              $result['Message'] = "Failed to change password";
          }
        }else{
          $result['status'] = true;
          $result['Message'] = "Password and conform password mismatch";
        }
      }else{
        $result['status'] = false;
        $result['Message'] = "Post date empty";
      }
      $this->RequestHandler->respondAs('json');
      $this->response->getType('application/json');
      echo json_encode($result);
    }


}
?>
