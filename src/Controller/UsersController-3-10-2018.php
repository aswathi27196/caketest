<?php
//namespace App\Controller;

//use App\Controller\AppController;

  namespace App\Controller;
  use App\Controller\AppController;
  use Cake\ORM\TableRegistry;
  use Cake\Datasource\ConnectionManager;
  use Cake\Auth\DefaultPasswordHasher;
  use Cake\EmailConfig;

/**
 * AddToCart Controller
 *
 * @property \App\Model\Table\AddToCartTable $AddToCart
 *
 * @method \App\Model\Entity\AddToCart[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(){
   		 parent::initialize();
       $this->loadComponent('RequestHandler');
       //$this->loadHelper('EncodeHelper');
   	}

    // public function beforeFilter(Event $event) {
    //    $this->Auth->allow(array('add'));
    // }

     public function view(){

        $user= $this->Users->find();
        $this->set('user', $user);
    }
   
    public function add(){ 
      //echo "fhgbfh";exit;
      if($this->request->is('post')){
       //echo "fhgbfh";exit;
          $name = $this->request->data('name');
          $email = $this->request->data('email');
          $password = password_hash($this->request->data('password'), PASSWORD_DEFAULT);
        if(!empty($this->request->data('upload'))){
          
          $file = $this->request->data('upload'); //put the data into a var for easy use
          $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
          $arr_ext = array('jpg', 'jpeg', 'gif');
          //only process if the extension is valid
          if(in_array($ext, $arr_ext)){

            move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
            $img =  $this->request->data['uploadimage']= $file['name'];
          }
        }
        //using model
        $users = $this->Users->newEntity();

        $users->name = $name;
        $users->email = $email;
        $users->password = $password;

        $users->profileimage = $img;
        if($this->Users->save($users)){
           
          $this->setAction('view');
        }
      
      }else{
        //echo "fghjh";exit;
      }
      
    }

    public function edit($id){
      
      $user = $this->Users->get($id, [
          'contain' => []
      ]);
      
     if($this->request->is(['patch', 'post', 'put'])){
       
          $user = $this->Users->patchEntity($user, $this->request->getData());

         if ($this->Users->save($user)) {
              $this->Flash->success(__('The retailer has been saved.'));

              return $this->redirect(['action' => 'view']);
          }
          $this->Flash->error(__('The retailer could not be saved. Please, try again.'));
          
      } 
      $this->set(compact('user'));
    }

    public function delete($id){ 
      
      $users = $this->Users->find()->where(['id'=>$id])->first(); 
      $delete_user =  $this->Users->delete($users); 
      if($delete_user){
        echo "User deleted successfully."; 
         return $this->redirect(['action' => 'view']);
        //$this->setAction('view'); 
      }else{
        echo "User deleted failed."; 
        
      }
       
    } 
    
    public function emailsend(){

      //$App::uses('CakeEmail', 'Network/Email');
      $email = new EmailConfig('smtp');
      $email->from(array('sender@example.com' => 'test'));
      $email->to(array('aswathi.shenll@gmail.com' => 'aswathi'));
      $email->subject('test mail');
      $email->emailFormat('both'); 
      // both = html + text.

      //$email->template('template');
      if ($email->send()) {
      echo "Message has been sent.";
      } else {
      echo $email->smtpError;
      } 
    }


}
?>
