<?php
//namespace App\Controller;

//use App\Controller\AppController;

  namespace App\View\Helper;
  use Cake\View\Helper;
  use Cake\View\Helper\View;
  

/**
 * AddToCart Controller
 *
 * @property \App\Model\Table\AddToCartTable $AddToCart
 *
 * @method \App\Model\Entity\AddToCart[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EncodeHelper extends Helper
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize(){
   		 parent::initialize();
       
   	}
    function base64_url_encode($input) {
     return strtr(base64_encode($input), '+/=', '._-');
    }

    function base64_url_decode($input) {
     return base64_decode(strtr($input, '._-', '+/='));
    }

    


}
?>
