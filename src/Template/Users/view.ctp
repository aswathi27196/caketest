<h1><a href="<?php echo BASE_URL ?>">Back</a></h1>
<div class="container">
    <div class="row">
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>S.No</th>
              <th>Name</th>
              <th>Email</th>
              <th>Action</th>
               <th>Profile Image</th>
             
            </tr>
          </thead>
          <tbody id="myTable">
          	<?php 
          	if ( ! empty($user) ){ 
          		foreach ($user as  $value) { ?>
          	<tr>
              <td><?= $value->id ?></td>
              <td><?= $value->name ?></td>
              <td><?= $value->email ?></td>
              <td><a href ="<?php echo BASE_URL .'/users/edit/'.base64_encode($value->id)  ?>" >Edit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|</a>
              <a href ="<?php echo BASE_URL .'/users/delete/'.$value->id  ?>" >Delete</a></td>
              <?php if($value->profileimage){ ?>
              <td><img src ="<?= BASE_URL . '/img/'.$value->profileimage ?>" ></a></td>
            <?php }else{ ?>
              <td><img src ="<?= BASE_URL . '/img/images.jpeg' ?>" ></a></td>
            <?php } ?>
            </tr>
             <?php	}} ?>
          </tbody>
        </table>   
      </div>
      <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg pager" id="myPager"></ul>
      </div>
	</div>
</div>
