<?php
//namespace App\Controller;

//use App\Controller\AppController;

  namespace App\Controller;
  use App\Controller\AppController;
  use Cake\ORM\TableRegistry;
  use Cake\Datasource\ConnectionManager;
  use Cake\Auth\DefaultPasswordHasher;
  use Cake\EmailConfig;
  use Cake\Event\Event;

/**
 * AddToCart Controller
 *
 * @property \App\Model\Table\AddToCartTable $AddToCart
 *
 * @method \App\Model\Entity\AddToCart[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
	  {
   		 parent::initialize();
        $this->loadModel('Users');
   	}

    public function beforeFilter(Event $event) {        
       $this->Auth->allow(array('login','forgotpassword','changepassword','add'));
    }

    public function view()
    {
      $user= $this->Users->find();
      $this->set('user', $user);
    }


    public function login(){

      if($this->request->is('post')){
        $user = $this->Auth->identify();
       if($user) {
          $this->Auth->setUser($user);
          $session = $this->request->session();
          $session->write('users',$user);
          $this->Flash->success(__('login successfully.'));
          $this->setAction('view');
        }else{
          $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
      }
    }


    public function forgotpassword(){
      if($this->request->is('post')) {
        $email = $this->request->data['email'];
        $user = $this->Users->find()->where(['email' => $email ])->first();
        $this->set(compact('user'));
          
        if(!$user) {
           $this->Flash->error(__('No user with that email found.'));
          $this->redirect(['controller' => 'Users','action' => 'forgotpassword']);
        }else{
          $this->set('user',$this->Users->find()->where(['email' => $email ])->first());
          $this->setAction('changepassword');
        }
      }
    }
  
    public function changepassword(){
      
      $email =  $this->request->data('email');
      $form_name =  $this->request->data('form-name');  
      
       if(($this->request->is(['post'])) && ($form_name == 'forgot-password')) {
         $id=  $this->Auth->user('id');
         $users = $this->Users->find()->where(['email' => $email ])->first();
         $this->set(compact('users'));
        }
        if(($this->request->is(['post'])) && ($form_name == 'change-password')) {
          $id=  $this->Auth->user('id');
          $users = $this->Users->find()->where(['email' => $email ])->first();
          $data = $this->request->getData();
       
          if($data['password'] ==  $data['confpass']){
          
             $users = $this->Users->patchEntity($users, $data);
            if ($this->Users->save($users)) {
              $this->Flash->success(__('Password changed Succesfully.'));
              return $this->redirect(['controller' => 'Users','action' => 'login']);
            }else{
                $this->Flash->error(__('Password changed failed.'));
                return $this->redirect(['controller' => 'Users','action' => 'changepassword']);
            }
         $this->set(compact('users'));
        }else{
          $this->Flash->error(__('Password missmath.'));
         return $this->redirect(['controller' => 'Users','action' => 'changepassword']);
        }
      }
    }
   
   
    public function add(){ 

      if($this->request->is('post')){
        $data = $this->request->getData();
        if(!empty($this->request->data('upload'))){
          
          $file = $this->request->data('upload'); //put the data into a var for easy use
          $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
          $arr_ext = array('jpg', 'jpeg', 'gif');
         
          //only process if the extension is valid
          if(in_array($ext, $arr_ext)){

            move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/' . $file['name']);
            $data['uploadimage'] = $file['name'];
          }
        }
        //using model
        $users = $this->Users->newEntity();
        $users = $this->Users->patchEntity($users,$data);
        if($this->Users->save($users)){
            $this->Flash->success(__('The user has been saved.'));
            $this->setAction('view');
        }else{
           $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
      }
    }

    
    public function edit($id){
      
      $user = $this->Users->get(base64_decode($id), [
          'contain' => []
      ]);
      
     
      if($this->request->is(['patch', 'post', 'put'])){
        
        $user = $this->Users->patchEntity($user, $this->request->getData());
        if($this->Users->save($user)) {
          $this->Flash->success(__('The retailer has been saved.'));
          return $this->redirect(['action' => 'view']);
        }
        $this->Flash->error(__('The retailer could not be saved. Please, try again.'));
      } 
      $this->set(compact('user'));
    }

    

    public function delete($id){ 
      $users = $this->Users->find()->where(['id'=>$id])->first(); 
      $delete_user =  $this->Users->delete($users); 
      if($delete_user){
        echo "User deleted successfully."; 
        return $this->redirect(['action' => 'view']);
      }else{
        echo "User deleted failed."; 
      }
    } 
    
     public function emailsend(){

      //$App::uses('CakeEmail', 'Network/Email');
      $email = new EmailConfig('smtp');
      $email->from(array('sender@example.com' => 'test'));
      $email->to(array('aswathi.shenll@gmail.com' => 'aswathi'));
      $email->subject('test mail');
      $email->emailFormat('both'); 
      // both = html + text.

      //$email->template('template');
      if ($email->send()) {
      echo "Message has been sent.";
      } else {
      echo $email->smtpError;
      } 

      /*$this->Email->to = 'Neil <aswathi.shenll@gmail.com>';
            $this->Email->subject = 'Cake test simple email';
            $this->Email->replyTo = 'neil6502@gmail.com';
            $this->Email->from = 'Cake Test Account <aswathi.shenll@gmail.com>';
            //Set the body of the mail as we send it.
            //Note: the text can be an array, each element will appear as a
            //seperate line in the message body.
            $res = $this->Email->send();
            if($res){
             echo "okkkkk";
              return $res;
            } else {
              echo "false";
            }*/
       /* $userName = 'test';
       // $email = new Email();
       
        $res= $EmailTransport->from('test@gmail.com')
            ->to('aswathi.shenll@gmail.com')
            ->subject('Contact')                   
            ->send();
            if($res){
                echo "mail sent";
            }else{
              echo "mail not sent";
            }*/
      /*$host = "ssl://smtp.gmail.com";
      $username = "aswathi.shenll@email.com";
      $password = "Shenll@2018";
      $port = "465";
      $recipients = "aswathi.shenll@email.com";
      $email = "test@gmail.com";
      $subject = "test";
       $headers = array ('From' => $email,
         'To' => $recipients,
         'Subject' => $subject);
       $smtp = Mail::factory('smtp',
         array ('host' => $host,
           'port' => $port,
           'auth' => true,
           'username' => $username,
           'password' => $password));

       $mail = new Mail;
       $mail = $smtp->send($recipients, $headers, $body);

       if (PEAR::isError($mail)) {
         echo("<p>" . $mail->getMessage() . "</p>");
        } else {
         echo("<p>Message successfully sent!</p>");
        }*/
    }


}
